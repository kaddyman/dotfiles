export PS1='\e\[\033[0;36m\]\w\e[0m \e\[\033[0;32m\]$(date -u "+%Y-%m-%d %H:%M:%S")\e[0m \e\[\033[0;33m\]$(git branch 2>/dev/null | grep '^*' | colrm 1 2)\e[0m \n $ '
#export PS1='\e\[\033[0;36m\]\w\e[0m $(terraform version | head -n1 | sed "s/Terraform //") \e\[\033[0;32m\]$(date -u "+%Y-%m-%d %H:%M:%S")\e[0m \e\[\033[0;33m\]$(git branch 2>/dev/null | grep '^*' | colrm 1 2)\e[0m \n $ '
export CLICOLOR=1
export PATH=${HOME}/repos/ops/bin:${PATH}
export EDITOR="vim"
export LC_COLLATE="C"
export SHELL="/usr/local/bin/bash"
export PATH="/usr/local/opt/libpq/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/libpq/lib"
export CPPFLAGS="-I/usr/local/opt/libpq/include"
export GOPATH="${HOME}/.go"
export GOROOT="$(brew --prefix golang)/libexec"
export PATH="$PATH:${GOPATH}/bin:${GOROOT}/bin"
export VAULT_ADDR=""
export GITHUB_TOKEN=""

alias j="cd ${HOME}/repos"
alias sbrc="source ${HOME}/.bashrc"
alias sub="/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl"
alias role-switcher="/Users/ken.addyman/repos/devops/scripts/aws/role-switcher"
alias tftime="date -u +'%Y%m%d-%H:%M:%S'"
alias c="cd"
alias ..="cd .."
alias ..="cd ../.."
alias ...="cd ../../.."
alias ....="cd ../../../../"

RESET='\033[0m'
RED='\033[00;31m'
GREEN='\033[00;32m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
PURPLE='\033[00;35m'
CYAN='\033[00;36m'
LIGHTGRAY='\033[00;37m'
LRED='\033[01;31m'
LGREEN='\033[01;32m'
LYELLOW='\033[01;33m'
LBLUE='\033[01;34m'
LPURPLE='\033[01;35m'
LCYAN='\033[01;36m'
WHITE='\033[01;37m'

function stealth() {
  kill -9 $(pgrep -f devops-[u]ser)
  kill -9 $(pgrep -f s[o]cks5)
  if [[ -d "${HOME}/proxy-profile" ]]; then rm -rf ${HOME}/proxy-profile; fi
  ssh -i ${HOME}/.ssh/lol.pem -D 1337 -q -C -N lol@lol.lol.lol &
  "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome" --user-data-dir="$HOME/proxy-profile" --proxy-server="socks5://localhost:1337" &
}

function cleanup() {
  kill -9 $(pgrep -f devops-[u]ser)
  kill -9 $(pgrep -f s[o]cks5)
  rm -rf ${HOME}/proxy-profile
}

function rainbowhack() {
  #(for i in {1..256}; do echo $(for j in $(seq 1 $COLUMNS); do echo -n $((RANDOM %2)); done | sed 's/\(.\{8\}\)./\1 /g'); done) | lolcat
  (for i in {1..256}; do echo $(for j in $(seq 1 $COLUMNS); do echo -n $((RANDOM %2)); done); done) | lolcat
}

function blah() {
  junkfile=$(mktemp)
  vim ${junkfile}
  rm -rf ${junkfile}
}

function mdr() {
  glow -s dark "${1:-}" | less -r
}

function fixdockertime() {
  docker run -it --rm --privileged --pid=host debian nsenter -t 1 -m -u -n -i date -u $(date -u +%m%d%H%M%Y)
}

function iplook() {
  # if you have maxmind api access and the python ip2geo script
  ${HOME}/repos/ip2geo/ip2geo --maxuserid %userid% --maxkey %key% --ip "${1:-}"
}

function ebash(){
  vim "${HOME}/.bashrc"
  sbrc
}

function geoip(){
  curl -s -X POST -H 'x-api-key: lol' -H 'Content-Type: application/json' -d'{"ip": "'${1:-}'"}' 'https://lol/geoip/prd/' | jq
}

function myip(){
  dig +short myip.opendns.com @resolver1.opendns.com
  curl -s -X POST -H 'x-api-key: lol' -H 'Content-Type: application/json' 'https://lol/ip/prd/' | jq -r '.Item.SourceIp'
}

function fsuse(){
  df -hP | column -t | grep -v ^none | ( read header ; echo "$header" | sed 's/Mounted.*on/Mounted_on/g' ; sort -rn -k 5)
}

function filefinder(){
  (find $(pwd) -type f -size +25M -print0 2> /dev/null | xargs -0 ls -lhsS) | column -t | cut -d ' ' -f 2- | sed -e 's/^[ \t]*//'
}

function sglookup() {
  aws ec2 describe-network-interfaces --filters Name=group-id,Values=${1:-}
}

function sftp-key-put() {
  key_name=${1:-}
  cat "${key_name}" | vault kv put secret/devops/sftp-keys/${key_name} value=-
}

function sftp-key-get() {
  key_name=${1:-}
  vault kv get -format=json secret/devops/sftp-keys/${key_name} | jq -r '.data.data.value'
}

function sftp-key-list() {
  vault kv list secret/devops/sftp-keys
}

function cgit() {
  # checkout master/main and pull
  if [[ "$PWD" =~ "repos/work/" ]]; then
    git checkout master
    git pull
  else
    git checkout main
    git pull
  fi
}

function fgit(){
  # fetch all remote branches
  git fetch --all
  git branch -a
}

function rgit() {
  # nuke option, will reset your repo like you just rm'd it and cloned it down again
  if [[ "$PWD" =~ "repos/work/" ]]; then
    cd $(git rev-parse --show-toplevel)
    git fetch origin
    git reset --hard origin/master
    git clean -fdx
  else
    cd $(git rev-parse --show-toplevel)
    git fetch origin
    git reset --hard origin/main
    git clean -fdx
  fi
}

function dgit() {
  # remove all local branches expect for master/main
  if [[ "$PWD" =~ "repos/work/" ]]; then
    git branch | grep -v "master" | xargs git branch -D
  else
    git branch | grep -v "main" | xargs git branch -D
  fi
}

function sgit() {
  # show files staged for push
  if [[ "$PWD" =~ "repos/work/" ]]; then
    git diff --stat --cached origin/master
  else
    git diff --stat --cached origin/main
  fi
}

function mgit() {
  # used when you want to update your branch with master/main
  if [[ "$PWD" =~ "repos/work/" ]]; then
    branch=$(git branch | grep \* | cut -d ' ' -f2)
    git checkout master
    git pull
    git checkout ${branch}
    git merge origin/master
  else
    branch=$(git branch | grep \* | cut -d ' ' -f2)
    git checkout main
    git pull
    git checkout ${branch}
    git merge origin/main
  fi
}

function jgit() {
  # jump to the root of your repo
  cd $(git rev-parse --show-toplevel)
}

function bgit() {
  # create a new branch, change to it and push it up
  # ex. bgit <YOUR_BRANCH_NAME>
  git branch "${1:-}"
  git checkout "${1:-}"
  git push -u origin "${1:-}"
}

function prgit() {
  # just run prgit in your branch to open a PR for it
  # made for github use but easy to modify
  if [[ -d "/Applications/Google Chrome.app" ]]; then
    INSTALLED="Google Chrome"
  elif [[ -d "/Applications/Firefox.app" ]]; then
    INSTALLED="firefox"
  elif [[ -d "/Applications/Safari.app" ]]; then
    INSTALLED="safari"
  else
    INSTALLED="none"
    printf "You don't have a supported browser (Chrome/Firefox/Safari) installed."
    exit 1
  fi

  repo=$(basename $(git rev-parse --show-toplevel))
  branch=$(git branch | grep \* | cut -d ' ' -f2)

  if [[ "$PWD" =~ "repos/work/" ]]; then
    open -a "${INSTALLED}" "https://github.com/sweetride/${repo}/pull/new/${branch}"
  else
    open -a "${INSTALLED}" "https://gitlab.com/kaddyman/${repo}/merge_requests/new?merge_request[source_branch]=${branch}&merge_request[target_branch]=main"
  fi
}

function ga() {
    # git add, if files is empty adds all
    if [ -z "$@" ]
    then
        git add .
    else
        git add "$@"
    fi
}

function gc() {
    # git commit, if message is empty, sets message to 'YOLO'
    if [ -z "$@" ]
    then
        git commit -m "YOLO"
    else
        git commit -m "$@"
    fi
}

function gp(){
    # git push
    git push
}

function gca() {
    # git adds all, commits with message 'YOLO', and pushes
    ga
    gc
    gp
}

function mfa-gen-auto() {
    # will generate the keys for your creds file if you have mfa on cli
    # update profile name and aws account id
    aws --profile %PROFILE% sts get-session-token --serial-number arn:aws:iam::%AWSACCOUNTID%:mfa/ken.addyman --token-code ${1:-} | jq -r '.Credentials | "aws_access_key_id=\(.AccessKeyId)\naws_secret_access_key=\(.SecretAccessKey)\naws_session_token=\(.SessionToken)"'
}

function ecr-login() {
  # AWS CLI V1 ecr login
  #$(aws ecr get-login --no-include-email)

  # If on mac be sure to update your shell in user account settings
  # otherwise bash v3 is used and v3 does not support declarative arrays
  # AWS CLI V2 ecr login
  searchstring=${1:-"null"}
  declare -A aws_accounts
  aws_accounts[kdc]="AWS_ACCOUNT_NUM"
  aws_accounts[kdd]="AWS_ACCOUNT_NUM"
  aws_accounts[kds]="AWS_ACCOUNT_NUM"
  if test "${aws_accounts[${searchstring}]+isset}"; then
    AWS_PROFILE=$searchstring aws ecr get-login-password | docker login --password-stdin --username AWS "${aws_accounts[$searchstring]}.dkr.ecr.us-east-2.amazonaws.com"
  else
    for x in "${!aws_accounts[@]}"; do printf "[%s]=%s\n" "$x" "${aws_accounts[$x]}" ; done
  fi
}

function ecs-logs() {
  # get cloudwatch logs from running ecs tasks
  ecs-cli logs --follow --task-id "${2:-}" --cluster "${1:-}" "${3:-}" | sed '/^$/d'
}

function sslg() {
  # get cert info
  cfssl certinfo -domain "${1:-}"
}

function sane() {
  # will print terraform 0.11 json in plans in a readable format
  echo "${1:-}" | sed "s/\\\n//g" | sed "s|\\\||g" | jq
}

function irm() {
  # delete file by inode number
  find . -inum "${1:-}" -exec rm -i {} \;
}

function gf() {
  # search recursively with a no case regex
  grep -inR --exclude-dir={.terraform,.git} ''"${1:-}"'' '.'
}

function dns() {
  # compare your local dns resolver to google/cloudflare
  printf "\n=================================\ngoogle dns 8.8.8.8 results\n\n"
  dig "${1:-google.com}" +short @8.8.8.8
  printf "\n=================================\ncloudflare dns 1.1.1.1 results\n\n"
  dig "${1:-google.com}" +short @1.1.1.1
  printf "\n=================================\nYour local dns results check resolv.conf\n\n"
  dig "${1:-google.com}" +short
  echo
}

function zipit() {
  # zip a file/folder
  # zipit file/ file.zip
  zip -r "${2}" "${1}" -x "*.DS_Store" -x "__MACOSX" -x "*._Filename"
}

function nosleep() {
  # keep your mac from sleeping
  caffeinate -dis
}

function aws-unset() {
    #This will unset all aws env vars
    unset AWS_ACCESS_KEY_ID
    unset AWS_SECRET_ACCESS_KEY
    unset AWS_SESSION_TOKEN
    unset AWS_REGION
    unset AWS_DEFAULT_REGION

    env | grep -q AWS
    if [[ $? -eq 0  ]]; then
      for i in $(env | grep AWS | awk -F'=' '{print $1}'); do
        if [[ ! "${i}" =~ "AWS_PROFILE" ]]; then
          echo "unsetting ${i}"
          unset ${i}
        fi
      done
    fi
    echo "all aws env variables except for AWS_PROFILE have been unset"
}

function aws-auth(){
    aws-unset

    # to create temp keys/token from a assumed role
    if [[ -z "${AWS_PROFILE:-}" ]]; then
      echo 'Please set $AWS_PROFILE.' >&2
      return 1
    fi

    if aws configure get source_profile &>/dev/null; then
      if [[ "$PWD" =~ "repos/work/" ]]; then
        region="us-east-2"
      else
        region="us-east-1"
      fi
      local creds source_profile arn

      source_profile=$(aws configure get source_profile)
      arn=$(aws configure get role_arn)
      role_name=$(echo "$arn" | awk -F'/' '{print $NF}')
      creds=$(AWS_PROFILE="$source_profile" aws sts assume-role --role-arn "$arn" --role-session-name "${USER}-${role_name}" --duration-seconds 3600)

      printf "\nYou local env has been setup with env variables for local use in ${AWS_PROFILE} if you want them. otherwise run aws-unset to clear them.\n\n"
      export AWS_ACCESS_KEY_ID="$(echo "$creds" | jq -r .Credentials.AccessKeyId)"
      export AWS_SECRET_ACCESS_KEY="$(echo "$creds" | jq -r .Credentials.SecretAccessKey)"
      export AWS_SESSION_TOKEN="$(echo "$creds" | jq -r .Credentials.SessionToken)"
      export AWS_REGION=${region}
      export AWS_DEFAULT_REGION=${region}

      printf "You can set these inside your app for temp access in ${AWS_PROFILE}. keys are good for 1 hour\n\n"
      echo "AWS_ACCESS_KEY_ID=$(echo $creds | jq -r .Credentials.AccessKeyId)"
      echo "AWS_SECRET_ACCESS_KEY=$(echo $creds | jq -r .Credentials.SecretAccessKey)"
      echo "AWS_SESSION_TOKEN=$(echo $creds | jq -r .Credentials.SessionToken)"
      echo "AWS_REGION=${region}"
      echo "AWS_DEFAULT_REGION=${region}"
    fi
}

function tf() {
  aws-unset
  # simple terraform wrapper with global tfvars support
  # tf profile init/plan/apply/import
  # only works for state rm when encased in quotes
  tfenv use 0.11.11
  if AWS_PROFILE=${1:-} aws configure get source_profile &>/dev/null; then
    local creds source_profile arn

    source_profile="$(AWS_PROFILE=${1:-} aws configure get source_profile)"
    arn="$(AWS_PROFILE=${1:-} aws configure get role_arn)"
    creds="$(AWS_PROFILE="$source_profile" aws sts assume-role --role-arn "$arn" --role-session-name "${USER}--terraform" --duration-seconds 3600)"

    export AWS_ACCESS_KEY_ID="$(echo "$creds" | jq -r .Credentials.AccessKeyId)"
    export AWS_SECRET_ACCESS_KEY="$(echo "$creds" | jq -r .Credentials.SecretAccessKey)"
    export AWS_SESSION_TOKEN="$(echo "$creds" | jq -r .Credentials.SessionToken)"
  fi

  if [[ "${2:-}" =~ "init" && -d "./.terraform" ]]; then
      echo "removing .terraform folder so caching stops making me angry"
      rm -rf .terraform/
  fi

  terraform fmt
  terraform "${2:-}" -var-file=~/.terrified/${1:-}.tfvars ${3:-} ${4:-}
}

function ta() {
  aws-unset
  tfenv use 0.13.0

  if [[ "${1:-}" =~ "init" && -d "./.terraform" ]]; then
      echo "removing .terraform folder so caching stops making me angry"
      rm -rf .terraform/
  fi

  terraform fmt
  if [[ "$PWD" =~ "repos/work/" ]]; then
    PROFILE_NAME="control"
  else
    PROFILE_NAME="ken"
  fi
  AWS_PROFILE="$PROFILE_NAME" terraform $@
}

function tap() {
  if [[ ! "${1:-lol}" =~ "dat" ]]; then
    AWS_PROFILE=control terraform plan -var-file=${HOME}/.terrified/control.tfvars
  else
    AWS_PROFILE=control terraform apply -var-file=${HOME}/.terrified/control.tfvars
  fi
}

function va {
  vault kv $@
}

function vl() {
  unset VAULT_TOKEN
  vault_role=${1:-devops}
  AWS_PROFILE=kdc aws-auth
  export VAULT_TOKEN=$(vault login -method=aws -token-only role="${vault_role}")
  aws-unset
}

function rsakey() {
  ssh-keygen -t rsa -b 4096 -m PEM -f "${1:-lol}.pem"
}

function ppk-gen() {
  puttygen ${1:-} -o ${1:-}.ppk -O private
}

function pubkey() {
  # generate pubkey from private
  ssh-keygen -y -f ${1:-}
}

# SSM Editor
# how to install vipe
# https://github.com/juliangruber/vipe
edit_ssm() {
  local path="${1:-}"
  local output="$(mktemp)"
  aws ssm get-parameters --name ${path} --with-decryption |
  jq -r '.Parameters[].Value' |
  base64 -D |
  vipe |
  jq . |
  base64 > "${output}"
  aws ssm put-parameter --name ${path} --type SecureString --value "$(cat "${output}")" --overwrite
  rm "${output}"
}

function cip() {
  (geoip ${1:-} | jq -r '. | "\(.isp),\(.ip_address)"'; geoip $(myip) | jq -r '. | "\(.isp),\(.ip_address)"') | column -t -s,
}
