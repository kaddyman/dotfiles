set mouse=v
set backspace=indent,eol,start
syntax on
set nu
:command No set nonumber
:command Nu set nu
set laststatus=2
set statusline+=%F\
set statusline+=col:\ %c
set ruler
:nnoremap <leader>ev :vsplit $MYVIMRC<cr>
:set tabstop=4
:set shiftwidth=4
:set expandtab
:set copyindent
:set preserveindent
:set softtabstop=0
:set autoindent
