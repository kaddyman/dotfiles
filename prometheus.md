# prometheus queries

just some general queries I want to save for node_exporter/cadvisor

```
ecs cluster dashboard

node_memory_MemFree_bytes{instance_name=~"$cluster"}
{{instance_id}}

count(container_start_time_seconds{instance_name=~"$cluster", container_label_com_amazonaws_ecs_container_name=~".*", image=~"[0-9].*"})
$cluster

100 - (avg by (instance_id) (irate(node_cpu_seconds_total{instance_name="$cluster",mode="idle"}[5m])) * 100)
{{instance_id}}

node_load5{instance_name=~"$cluster"}
{{instance_id}}

node_filesystem_avail_bytes{instance_name=~"$cluster"}
{{instance_id}}

Variables
label_values(node_uname_info{job=~".*"}, instance_name)
/.*-ecs/

---

deep learning node

up{instance_name=~"deep-learning-.*"}
{{instance_name}} {{instance}}

process_start_time_seconds{instance_name=~".*deep-learning.*"}*1000
{{instance_name}} {{instance}}

---

ecs container start time

container_start_time_seconds{instance_name=~"$cluster", container_label_com_amazonaws_ecs_container_name=~".*",image=~"[0-9].*"} * 1000
{{container_label_com_amazonaws_ecs_container_name}}

variables
label_values(node_uname_info{job=~".*"}, instance_name)

---

ecs service metrics

container_start_time_seconds{instance_name=~"$instance_name", container_label_com_amazonaws_ecs_container_name=~"$container_name",image=~"[0-9].*"} * 1000
{{container_label_com_amazonaws_ecs_container_name}}

container_memory_usage_bytes{instance_name=~"$instance_name", container_label_com_amazonaws_ecs_container_name=~"$container_name", id=~"/ecs/.*"}
{{$container_name}}

rate(container_cpu_usage_seconds_total{instance_name=~"$instance_name", container_label_com_amazonaws_ecs_container_name=~"$container_name", id=~"/ecs/.*"}[1m])
{{$container_name}}

container_fs_writes_bytes_total{instance_name=~"$instance_name", container_label_com_amazonaws_ecs_container_name=~"$container_name", id=~"/ecs/.*"}
{{$container_name}}

variables
container_name
container_memory_usage_bytes
/container_label_com_amazonaws_ecs_task_definition_family="(\b[^\s=]+)"/

instance_name
label_values(container_memory_usage_bytes, instance_name)

---

rds metrics
aws_rds_cpuutilization_average{dbinstance_identifier="$database", island="$island"} offset 10m
aws_rds_burst_balance_average{dbinstance_identifier="$database", island="$island"} offset 10m
aws_rds_cpucredit_usage_average{dbinstance_identifier="$database", island="$island"} offset 10m
aws_rds_database_connections_average{dbinstance_identifier="$database", island="$island"} offset 10m

aws_rds_read_iops_average{dbinstance_identifier="$database", island="$island"} offset 10m
aws_rds_write_iops_average{dbinstance_identifier="$database", island="$island"} offset 10m

aws_rds_freeable_memory_average{dbinstance_identifier="$database", island="$island"} offset 10m
aws_rds_free_storage_space_average{dbinstance_identifier="$database", island="$island"} offset 10m
```
